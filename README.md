[![Netlify Status](https://api.netlify.com/api/v1/badges/7a1fc15d-7ccc-45d9-ad1e-17774f36ff2a/deploy-status)](https://app.netlify.com/sites/gallant-hopper-1c73f5/deploys)

# Clickonrefresh Portfolio Dashboard

# How to run and deploy this project

```
npm run build
git add .
git commit -m "initial commit"
git push -u origin main
npm install netlify-cli -g
netlify deploy
netlify deploy --prod
```

