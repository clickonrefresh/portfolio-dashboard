import React from 'react';
import { DiAws, DiDocker, DiFirebase, DiNginx, DiNodejs, DiPython, DiReact, DiUbuntu, DiUnitySmall, DiZend } from 'react-icons/di';
import { Section, SectionDivider, SectionText, SectionTitle } from '../../styles/GlobalComponents';
import { List, ListContainer, ListItem, ListParagraph, ListTitle } from './TechnologiesStyles';

const Technologies = () => (
  <Section id="tech">
    <SectionDivider />
    <SectionTitle>Tech</SectionTitle>
    <SectionText>
      I work with a range of full stack technologies with a key focus on 3D Web Development, Node based frameworks, Python, Linux, Containerisation & Hosting.
    </SectionText>
    <List>
      <ListItem>
        <DiReact size="3rem" />
        <ListContainer>
          <ListTitle>Front-End</ListTitle>
          <ListParagraph>
            Experience with <br />
            React.js and Three.js
          </ListParagraph>
        </ListContainer>
      </ListItem>
      <ListItem>
        <DiNodejs size="3rem" />
        <ListContainer>
          <ListTitle>Back-End</ListTitle>
          <ListParagraph>
            Experience with <br />
            Node.js, Nginx, Docker
          </ListParagraph>
        </ListContainer>
        <DiNginx size="2rem" />
        <DiDocker size="2rem" />
      </ListItem>
      <ListItem>
        <DiZend size="3rem" />
        <ListContainer>
          <ListTitle>UI/UX</ListTitle>
          <ListParagraph>
            Experience with <br />
            tools like Webflow, Figma and Storybook
          </ListParagraph>
        </ListContainer>
      </ListItem>
      <ListItem>
        <DiUnitySmall size="3rem" />
        <ListContainer>
          <ListTitle>Game Engines & 3D Modelling</ListTitle>
          <ListParagraph>
            Experience with <br />
            Unreal Engine, Blender and Unity. <br />
            I use tools such as Sketchfab and Canva.
          </ListParagraph>
        </ListContainer>
      </ListItem>
      <ListItem>
        <DiPython size="3rem" />
        <ListContainer>
          <ListTitle>Platform Development</ListTitle>
          <ListParagraph>
            From the start <br />
            of my I.T. path <br />
            I have used Odoo ce as a means <br />
            of learning development <br />
            and freelance work.
          </ListParagraph>
        </ListContainer>
      </ListItem>
      <ListItem>
        <DiUbuntu size="3rem" />
        <DiAws size="3rem" />
        <ListContainer>
          <ListTitle>Various</ListTitle>
          <ListParagraph>
            Adept in <br />
            Ubuntu Linux Server & Desktop <br />
            I use platforms like Gitlab, Github, Netlify, AWS, Oracle Cloud, and more for my code hosting and CICD.
          </ListParagraph>
        </ListContainer>
      </ListItem>
    </List>
  </Section>
);

export default Technologies;
