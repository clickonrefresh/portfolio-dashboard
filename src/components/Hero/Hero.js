import React from 'react';

import { Section, SectionText, SectionTitle } from '../../styles/GlobalComponents';
import Button from '../../styles/GlobalComponents/Button';
import { LeftSection } from './HeroStyles';

import { useRouter } from 'next/router'

const Hero = () => {
  const router = useRouter()
  return(
  <Section row nopadding>
    <LeftSection>
      <SectionTitle main center>
        You Made It! <br />
        Don't Panic!
      </SectionTitle>
      <SectionText>
        The only way out is through!
      </SectionText>
      <Button onClick={() => router.push("/#projects")}>Go!</Button>
    </LeftSection>

  </Section>
)};

export default Hero;